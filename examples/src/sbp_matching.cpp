/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>

#include <boost/lexical_cast.hpp>
#include <boost/timer.hpp>
#include <flann/flann.h>

#include <pcl/point_types.h>
#include <pcl/correspondence.h>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transformation_from_correspondences.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "pcl/features/sbp.h"
#include "pcl/keypoints/sbp_keypoint.h"

typedef pcl::PointXYZ PointType;
typedef pcl::SBPSignature64 OutType;

int scale= 14;
int dist_threshold = 0;

void 
printUsage (const char* progName)
{
  std::cout << "\n\nUsage: " << progName << " [options] <object1.pcd> <object2.pcd>\n\n"
  << "Options:\n"
  << "-------------------------------------------\n"
  << "-h           this help\n"
  << "-k           perform sbp keypoint detection\n"
  << "-wacv        use cloud dimensions (WACV 2016)\n"
  << "-s <int>     scale (default " << scale << ")\n"
  << "-d <int>     distance threshold for matching (default " << dist_threshold << ")\n"
  << "-v           visualize output\n"
  << "\n\n";
}

int
estimateSBP(const pcl::PointCloud<PointType>::ConstPtr &cloud, pcl::PointCloud<OutType>::Ptr &sbph, bool use_keypoints, pcl::PointCloud<PointType>::Ptr &sampled_cloud, bool use_cloud_dimensions, double support_radius = 0.01f)
{
  boost::shared_ptr<std::vector<int> > keypoint_indices (new std::vector<int>);
  
  if (use_keypoints)
  {  
    pcl::SBPKeypoint<PointType> sbp_keypoints;
    sbp_keypoints.setInputCloud (cloud);
//    sbp_keypoints.setUseCloudDimensions (use_cloud_dimensions);
    sbp_keypoints.setRadiusSearch (support_radius); // If use_cloud_dimensions is set to true, this param will be ignored
    sbp_keypoints.setSelectionMethod (pcl::SBPKeypoint<PointType>::SBP_N, 30); // N30
    
    pcl::PointCloud<int> keypoints_cloud;
    sbp_keypoints.compute (keypoints_cloud);
    
    keypoint_indices->reserve (keypoints_cloud.size ());
    sampled_cloud->reserve (keypoints_cloud.size ());
    for (pcl::PointCloud<int>::iterator it = keypoints_cloud.begin (); it != keypoints_cloud.end (); ++it)
    {
      keypoint_indices->push_back (*it);
      sampled_cloud->push_back (cloud->points[*it]);
    }
  }
  else
    pcl::copyPointCloud (*cloud, *sampled_cloud);
  
  // Create the SBP estimation class and pass the input dataset
  pcl::SBPEstimation<PointType, OutType> sbp;

  sbp.setInputCloud (cloud);
  if (use_keypoints)
    sbp.setIndices (keypoint_indices);
  sbp.setUseCloudDimensions (use_cloud_dimensions);
  sbp.setRadiusSearch (support_radius); // If use_cloud_dimensions is set to true, this param will be ignored
  
  // Compute the features
  sbp.compute (*sbph);
  
  return 0;
}

double
getCloudResolution (const pcl::PointCloud<PointType>::ConstPtr &cloud)
{
  // Calculate cloud resolution: http://www.pointclouds.org/documentation/tutorials/correspondence_grouping.php
  double cloud_resolution = 0;
  int n_points = 0;
  int nres;
  std::vector<int> indices (2);
  std::vector<float> sqr_distances (2);
  pcl::search::KdTree<PointType> tree;
  tree.setInputCloud (cloud);
  
  for (size_t i = 0; i < cloud->size (); ++i)
  {
    if (!pcl_isfinite (cloud->points[i].x))
      continue;

    //Considering the second neighbor since the first is the point itself.
    nres = tree.nearestKSearch (i, 2, indices, sqr_distances);
    if (nres == 2)
    {
      cloud_resolution += sqrt (sqr_distances[1]);
      ++n_points;
    }
  }
  if (n_points != 0)
    cloud_resolution /= n_points;
        
  return cloud_resolution;
}

int 
main (int argc, char** argv)
{
  // ---- Parse Command Line Arguments ---- //
  
  if (pcl::console::find_argument (argc, argv, "-h") >= 0)
  {
    printUsage (argv[0]);
    return 0;
  }
  
  bool extract_keypoints = false;
  if (pcl::console::find_argument (argc, argv, "-k") >= 0)
  {
    extract_keypoints = true;
    std::cout << "Extract SBP keypoints." << std::endl;
  }
  
  bool use_cloud_dimensions = false;
  if (pcl::console::find_argument (argc, argv, "-wacv") >= 0)
  {
    use_cloud_dimensions = true;
    std::cout << "Use cloud dimensions for SBP generation (WACV 2016 version). Scale will be ignored." << std::endl;
  }
  
  if (pcl::console::parse (argc, argv, "-s", scale) != -1)
    std::cout << "Setting scale to " << scale << "." << std::endl;
  
  if (pcl::console::parse (argc, argv, "-d", dist_threshold) != -1)
    std::cout << "Setting hamming distance threshold to " << dist_threshold << "." << std::endl;

  bool visualize = false;
  if (pcl::console::find_argument (argc, argv, "-v") >= 0)
    visualize = true;
  
  
  // ---- Read pcd files ---- //
  
  std::cout << "Load PCD files." << std::endl;
  
  pcl::PointCloud<PointType>::Ptr cloud1 (new pcl::PointCloud<PointType>);
  pcl::PointCloud<PointType>::Ptr cloud2 (new pcl::PointCloud<PointType>);
  std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "pcd");
  if (pcd_filename_indices.size () == 2)
  {
    std::string filename1 = argv[pcd_filename_indices[0]];
    if (pcl::io::loadPCDFile (filename1, *cloud1) == -1)
    {
      std::cerr << "Was not able to open file '" << filename1 << "'." << std::endl;
      printUsage (argv[0]);
      return 0;
    }
    std::cout << "Loaded " << cloud1->size () << " points from file '" << filename1 << "'." << std::endl;
    
    std::string filename2 = argv[pcd_filename_indices[1]];
    if (pcl::io::loadPCDFile (filename2, *cloud2) == -1)
    {
      std::cerr << "Was not able to open file '" << filename2 << "'." << std::endl;
      printUsage (argv[0]);
      return 0;
    }
    std::cout << "Loaded " << cloud2->size () << " points from file '" << filename2 << "'." << std::endl;
  }
  else
  {
    PCL_ERROR ("Wrong input arguments.");
    printUsage (argv[0]);
    return 0;
  }
  
  double cloud_resolution = getCloudResolution (cloud1);
  double support_radius = cloud_resolution * scale;
  
  
  // ---- Process cloud 1 ---- //
  
  std::cout << "Processing cloud 1..." << std::endl;
  
  pcl::PointCloud<OutType>::Ptr sbph1 (new pcl::PointCloud<OutType> ());
  pcl::PointCloud<PointType>::Ptr sampled_cloud1 (new pcl::PointCloud<PointType> ());
  
  boost::timer t1;
  estimateSBP (cloud1, sbph1, extract_keypoints, sampled_cloud1, use_cloud_dimensions, support_radius);
  double elapsed_t1 = t1.elapsed();
  std::cout << "Cloud1 SBP estimation" << (extract_keypoints ? " and keypoint detection " : " ") << "time " << elapsed_t1 << std::endl;
  std::cout << "Found " << sbph1->points.size () << " SBP descriptors." << std::endl;
  
  
  // ---- Process cloud 2 ---- //
  
  std::cout << "Processing cloud 2..." << std::endl;
  
  pcl::PointCloud<OutType>::Ptr sbph2 (new pcl::PointCloud<OutType> ());
  pcl::PointCloud<PointType>::Ptr sampled_cloud2 (new pcl::PointCloud<PointType> ());
  
  boost::timer t2;
  estimateSBP (cloud2, sbph2, extract_keypoints, sampled_cloud2, use_cloud_dimensions, support_radius);
  double elapsed_t2 = t2.elapsed();
  std::cout << "Cloud2 SBP estimation" << (extract_keypoints ? " and keypoint detection " : " ") << "time " << elapsed_t2 << std::endl;
  std::cout << "Found " << sbph2->points.size () << " SBP descriptors." << std::endl;
  
  // ---- Matching ---- //
  
  // FLANN Search http://www.pointclouds.org/assets/rss2011/06_search.pdf
  // FLANN Manual http://www.cs.ubc.ca/research/flann/uploads/FLANN/flann_manual-1.8.4.pdf
  
  int n1 = sbph1->size ();
  int n2 = sbph2->size ();
  int descSize = OutType::descriptorSize ();
  int nn = 2;
  
  flann::Matrix<unsigned char> data (new unsigned char[n1 * descSize], n1, descSize);
  flann::Matrix<unsigned char> query (new unsigned char[n2 * descSize], n2, descSize);
  
  // populate the matrix with features - one feature/row
  for (int i = 0; i < n1; i++)
    for (int j = 0; j < descSize; j++)
      data[i][j] = sbph1->points[i].pattern[j];
  
  for (int i = 0; i < n2; i++)
    for (int j = 0; j < descSize; j++)
      query[i][j] = sbph2->points[i].pattern[j];
  
  flann::Matrix<int> nn_indices (new int[query.rows * nn], query.rows, nn);
  flann::Matrix<unsigned int> nn_distances (new unsigned int[query.rows * nn], query.rows, nn);
  
  boost::timer t3;
//  flann::Index< flann::Hamming<unsigned char> > index(data, flann::LshIndexParams());
  flann::Index< flann::Hamming<unsigned char> > index(data, flann::HierarchicalClusteringIndexParams());
  index.buildIndex();
  
  // do a knn search, using 128 checks
  index.knnSearch (query, nn_indices, nn_distances, nn, flann::SearchParams(128));
  double elapsed_t3 = t3.elapsed();
  std::cout << "Matching time " << elapsed_t3 << std::endl;
  
//  flann::save_to_file(nn_indices,"result.hdf5","result");
  
  pcl::Correspondences correspondences;
  correspondences.resize (query.rows * nn);
  int nr_matches = 0;
  int nr_candidates = 0;
  int nr_unique_matches = 0;
  for (int i = 0; i < query.rows; i++)
  {    
    if (nn_distances[i][0] <= dist_threshold && nn_distances[i][1] != 0)
      {
          pcl::Correspondence c;
          c.index_query = nn_indices[i][0];
          c.index_match = i;
          c.distance = nn_indices[i][0] / nn_distances[i][1];
          c.weight = 1 - c.distance;
          correspondences[nr_matches++] = c;
      }
  }
  correspondences.resize (nr_matches);
  std::cout << "Nr matches: " << nr_matches << std::endl;

  delete[] data.ptr();
  delete[] query.ptr();
  delete[] nn_indices.ptr();
  delete[] nn_distances.ptr();

  
  // ---- Transformation ---- //
  
  pcl::TransformationFromCorrespondences trans;
  for (int i = 0; i < correspondences.size (); i++) {
    trans.add (sampled_cloud1->points[correspondences[i].index_query].getVector3fMap (), sampled_cloud2->points[correspondences[i].index_match].getVector3fMap (), correspondences[i].weight);
  }
  Eigen::Affine3f transformation = trans.getTransformation ();
  std::cout << transformation.matrix () << std::endl;
  
  
  // ---- Visualization ---- //
  
  if (visualize)
  {
    // Visualize matches
    pcl::visualization::PCLVisualizer viewer_matching ("Matching");
    viewer_matching.setBackgroundColor (1, 1, 1); // White
    
    pcl::visualization::PointCloudColorHandlerCustom<PointType> cloud1_color (cloud1, 150, 150, 150); // Grey
    viewer_matching.addPointCloud (cloud1, cloud1_color, "cloud1");
    
    pcl::PointCloud<PointType>::Ptr cloud2_t (new pcl::PointCloud<PointType>);
    pcl::PointCloud<PointType>::Ptr sampled_cloud2_t (new pcl::PointCloud<PointType>);
    Eigen::Array3f offset (support_radius * 20, 0.0f, 0.0f);
    Eigen::Quaternion<float> quaternion (0.0f, 0.0f, 0.0f, 0.0f);
    pcl::transformPointCloud (*cloud2, *cloud2_t, offset, quaternion);
    pcl::transformPointCloud (*sampled_cloud2, *sampled_cloud2_t, offset, quaternion);
    pcl::visualization::PointCloudColorHandlerCustom<PointType> cloud2_t_color (cloud2_t, 255, 255, 102); // Light Yellow
    viewer_matching.addPointCloud (cloud2_t, cloud2_t_color, "cloud2");
    
    viewer_matching.addCorrespondences<PointType> (sampled_cloud1, sampled_cloud2_t, correspondences, "matching");
    
    while (!viewer_matching.wasStopped ()) // Display the visualiser until 'q' key is pressed
      viewer_matching.spinOnce ();
    
    // Visualize registration
    pcl::visualization::PCLVisualizer viewer_registration ("Registration");
    viewer_registration.setBackgroundColor (1, 1, 1); // White
    
    pcl::visualization::PointCloudColorHandlerCustom<PointType> cloud2_color (cloud2, 255, 255, 102); // Light Yellow
    viewer_registration.addPointCloud (cloud2, cloud2_color, "cloud_original");

    pcl::PointCloud<PointType>::Ptr cloud1_t (new pcl::PointCloud<PointType>);
    pcl::transformPointCloud (*cloud1, *cloud1_t, transformation);
    pcl::visualization::PointCloudColorHandlerCustom<PointType> cloud1_t_color (cloud1_t, 150, 150, 150); // Grey
    viewer_registration.addPointCloud (cloud1_t, cloud1_t_color, "cloud_transformed");

    while (!viewer_registration.wasStopped ()) { // Display the visualiser until 'q' key is pressed
      viewer_registration.spinOnce ();
    }
  }
}
