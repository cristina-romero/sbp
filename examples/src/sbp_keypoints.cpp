/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>

#include <boost/lexical_cast.hpp>

#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/console/parse.h>
#include <pcl/io/ply_io.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "pcl/keypoints/sbp_keypoint.h"

typedef pcl::PointXYZRGB PointType;

double
getMeshResolution (const boost::shared_ptr<pcl::PolygonMesh> &mesh)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr mesh_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2(mesh->cloud, *mesh_cloud);
  
  Eigen::VectorXf edges_length (mesh->polygons.size () * 3);
  
  for (int i = 0; i < mesh->polygons.size (); i++)
  {
    pcl::Vertices v = mesh->polygons[i];
    
    if (v.vertices.size () != 3)
    {
      std::cerr << "[getMeshResolution] Found " << v.vertices.size () << " vertices in polygon mesh (should be a triangle)." << std::endl;
      edges_length[i * 3] = std::numeric_limits<float>::max ();
      edges_length[i * 3 + 1] = std::numeric_limits<float>::max ();
      edges_length[i * 3 + 2] = std::numeric_limits<float>::max ();
      continue;
    }
    
    for (int j = 0; j < v.vertices.size (); j++)
    {
      int k = (j == v.vertices.size () - 1) ? 0 : j + 1;
      edges_length[i * 3 + j] = (mesh_cloud->points[v.vertices[j]].getVector4fMap () - mesh_cloud->points[v.vertices[k]].getVector4fMap ()).norm ();
    }
  }
  
  double mesh_resolution = edges_length.mean ();
  return mesh_resolution;
}


void 
printUsage (const char* progName)
{
  std::cout << "\n\nUsage: " << progName << " [options] <input.ply>\n\n"
  << "Options:\n"
  << "-------------------------------------------\n"
  << "-h           this help\n"
  << "-m <int>     selection method [1:frequency, 2: min uniform pattern idx, 3: number of patterns, 4: number of points]\n"
  << "-p <int>     selection method param\n"
  << "-s <float>   scale\n"
  << "-c <string>  camera parameters for screenshot\n"
  << "-v           visualize output\n"
  << "\n\n";
}

int 
main (int argc, char** argv)
{
  // ---- Parse command line arguments ---- //
  
  if (pcl::console::find_argument (argc, argv, "-h") >= 0)
  {
    printUsage (argv[0]);
    return 0;
  }
  
  int selection_method = -1;
  if (pcl::console::parse (argc, argv, "-m", selection_method) == -1)
  {
    std::cerr << "Selection method not set." << std::endl;
    printUsage (argv[0]);
    return 1;
  }
  std::string method_name = "";
  switch (selection_method)
  {
    case 1:
      method_name = "f";
      break;
    case 2:
      method_name = "nmin";
      break;
    case 3:
      method_name = "n";
      break;
    case 4:
      method_name = "m";
      break;
    default:
      std::cerr << "Invalid selection method." << std::endl;
      printUsage (argv[0]);
      return 1;
  }
  std::cout << "Setting selection method to " << selection_method << "." << std::endl;
  
  int selection_method_param = -1;
  if (pcl::console::parse (argc, argv, "-p", selection_method_param) == -1)
  {
    std::cerr << "Selection method param not set." << std::endl;
    printUsage (argv[0]);
    return 1;
  }
  std::cout << "Setting selection method param to " << selection_method_param << "." << std::endl;
  
  float scale = 0.0f;
  if (pcl::console::parse (argc, argv, "-s", scale) == -1)
  {
    std::cerr << "Scale not set." << std::endl;
    printUsage (argv[0]);
    return 1;
  }
  std::cout << "Setting scale to " << scale << "." << std::endl;
  
  bool visualize = false;
  if (pcl::console::find_argument (argc, argv, "-v") >= 0)
    visualize = true;

  std::string camera_file = "";
  bool save_screenshot = false;
  if (pcl::console::parse (argc, argv, "-c", camera_file) != -1)
  {
    std::cout << "Create screenshot with camera parameters from file '" << camera_file << "'." << std::endl;
    save_screenshot = true;
  }
  
  
  // ---- Read ply file ---- //
  
  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh);
  std::vector<int> ply_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "ply");
  std::string image_name;
  if (!ply_filename_indices.empty ())
  {
    std::string filename = argv[ply_filename_indices[0]];
    boost::filesystem::path ply_path (filename);
    image_name = ply_path.stem().c_str();

    if (pcl::io::loadPLYFile (filename, *mesh) == -1)
    {
      std::cerr << "Was not able to open file '" << filename << "'." << std::endl;
      return 1;
    }
    pcl::fromPCLPointCloud2 (mesh->cloud, *cloud);
    std::cout << "Loaded " << cloud->size () << " points from cloud '" << filename << "'." << std::endl;
  }
  else
  {
    std::cerr << "No PLY file provided." << std::endl;
    printUsage (argv[0]);
    return 1;
  }
  
  
  // ---- Calculate mesh resolution ---- //
  
  double mesh_resolution = getMeshResolution (mesh);
  std::cout << "Calculated mesh resolution: " << mesh_resolution << std::endl;
  
  
  // ---- Extract keypoints ---- //
  
  pcl::SBPKeypoint<PointType> sbp;
  sbp.setInputCloud (cloud);
  sbp.setRadiusSearch (mesh_resolution * scale);
  sbp.setSelectionMethod ((pcl::SBPKeypoint<PointType>::SelectionMethod) selection_method, selection_method_param);
  
  pcl::PointCloud<int> keypoint_indices;
  sbp.compute (keypoint_indices);
  
  std::cout << "Found " << keypoint_indices.points.size () << " keypoints with SBP detector.\n";
  
  
  // ---- Visualization ---- //
  
  if (visualize || save_screenshot)
  {
    pcl::PointCloud<PointType>::Ptr keypoints_cloud (new pcl::PointCloud<PointType>);
    pcl::copyPointCloud (*cloud, keypoint_indices.points, *keypoints_cloud);
    
    pcl::visualization::PCLVisualizer viewer ("SBP Keypoints");
    viewer.setBackgroundColor (1.0f, 1.0f, 1.0f); // White background
    
//    viewer.addPointCloud<PointType> (cloud, "cloud");
    viewer.addPolygonMesh (*mesh, "mesh");
    pcl::visualization::PointCloudColorHandlerCustom<PointType> keypoints_color_handler (keypoints_cloud, 255, 0, 0);
    viewer.addPointCloud (keypoints_cloud, keypoints_color_handler, "keypoints");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "keypoints");
    
    if (save_screenshot)
    {
      std::string screenshot_file = "./" + image_name + "-" + method_name + boost::lexical_cast<std::string> (selection_method_param) + ".png";
      viewer.loadCameraParameters (camera_file);
      sleep (1); // Wait to render
      viewer.saveScreenshot (screenshot_file);
      std::cout << "Saved screenshot '" << screenshot_file << "'." << std::endl;
      
      if (!visualize)
        viewer.close ();
    }
    
    while (!viewer.wasStopped ())
      viewer.spinOnce ();
  }
  
  return 0;
}
