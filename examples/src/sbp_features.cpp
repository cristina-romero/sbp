/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>

#include <boost/make_shared.hpp>

#include <pcl/point_types.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h> 
#include <pcl/io/ply_io.h> 

#include "pcl/keypoints/sbp_keypoint.h"
#include "pcl/features/sbp.h"

typedef pcl::PointXYZ PointType;
typedef pcl::SBPSignature64 OutType; // pcl::SBPSignature27

int scale = 14;

double
getMeshResolution (const boost::shared_ptr<pcl::PolygonMesh> &mesh)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr mesh_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2(mesh->cloud, *mesh_cloud);
  
  Eigen::VectorXf edges_length (mesh->polygons.size () * 3);
  
  for (int i = 0; i < mesh->polygons.size (); i++)
  {
    pcl::Vertices v = mesh->polygons[i];
    
    if (v.vertices.size () != 3)
    {
      std::cerr << "[getMeshResolution] Found " << v.vertices.size () << " vertices in polygon mesh (should be a triangle)." << std::endl;
      edges_length[i * 3] = std::numeric_limits<float>::max ();
      edges_length[i * 3 + 1] = std::numeric_limits<float>::max ();
      edges_length[i * 3 + 2] = std::numeric_limits<float>::max ();
      continue;
    }
    
    for (int j = 0; j < v.vertices.size (); j++)
    {
      int k = (j == v.vertices.size () - 1) ? 0 : j + 1;
      edges_length[i * 3 + j] = (mesh_cloud->points[v.vertices[j]].getVector4fMap () - mesh_cloud->points[v.vertices[k]].getVector4fMap ()).norm ();
    }
  }
  
  double mesh_resolution = edges_length.mean ();
  return mesh_resolution;
}

double
getCloudResolution (const pcl::PointCloud<PointType>::ConstPtr &cloud)
{
  // Calculate cloud resolution: http://www.pointclouds.org/documentation/tutorials/correspondence_grouping.php
  double cloud_resolution = 0;
  int n_points = 0;
  int nres;
  std::vector<int> indices (2);
  std::vector<float> sqr_distances (2);
  pcl::search::KdTree<PointType> tree;
  tree.setInputCloud (cloud);
  
  for (size_t i = 0; i < cloud->size (); ++i)
  {
    if (!pcl_isfinite (cloud->points[i].x))
      continue;

    //Considering the second neighbor since the first is the point itself.
    nres = tree.nearestKSearch (i, 2, indices, sqr_distances);
    if (nres == 2)
    {
      cloud_resolution += sqrt (sqr_distances[1]);
      ++n_points;
    }
  }
  if (n_points != 0)
    cloud_resolution /= n_points;
        
  return cloud_resolution;
}

void 
printUsage (const char* progName)
{
  std::cout << "\n\nUsage: " << progName << " [options] <input.pcd|input.ply>\n\n"
  << "Options:\n"
  << "-------------------------------------------\n"
  << "-h           this help\n"
  << "-k           perform sbp keypoint detection\n"
  << "-wacv        use cloud dimensions (WACV 2016), scale will be ignored\n"
  << "-s <int>     scale (default " << scale << ")\n"
  << "-o <string>  output path\n"
  << "\n\n";
}

int 
main (int argc, char** argv)
{
  // ---- Parse Command Line Arguments ---- //
  
  if (pcl::console::find_argument (argc, argv, "-h") >= 0)
  {
    printUsage (argv[0]);
    return 0;
  }

  bool use_keypoints = false;
  if (pcl::console::find_argument (argc, argv, "-k") >= 0)
  {
    std::cout << "Using SBP keypoint detection." << std::endl;
    use_keypoints = true;
  }

  bool use_cloud_dimensions = false;
  if (pcl::console::find_argument (argc, argv, "-wacv") >= 0)
  {
    std::cout << "Using cloud dimensions (WACV 2016 version). Scale will be ignored." << std::endl;
    use_cloud_dimensions = true;
  }

  if (pcl::console::parse (argc, argv, "-s", scale) != -1)
    std::cout << "Setting scale to " << scale << "." << std::endl;

  bool save_output = false;
  std::string output;
  if (pcl::console::parse (argc, argv, "-o", output) != -1)
  {
    boost::filesystem::path dir (output);
    if(!boost::filesystem::exists (dir))
    {
      std::cout << "Output dir '" << output << "' doesn't exist. " << std::endl;
      if (boost::filesystem::create_directory(dir))
        std::cout << "...successfully created!" << std::endl;
    }
    
    std::cout << "Output will be saved in '" << output << "'." << std::endl;
    save_output = true;
  }


  // ---- Read image file ---- //
  
  std::string filename, image_name;
  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "pcd");
  std::vector<int> ply_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "ply");
  double resolution = 0;
  if (!pcd_filename_indices.empty ())
  {
    filename = argv[pcd_filename_indices[0]];    
    boost::filesystem::path pcd_path (filename);
    image_name = pcd_path.stem().c_str();
    
    if (pcl::io::loadPCDFile (filename, *cloud) == -1)
    {
      std::cerr << "Was not able to open file '" << filename << "'." << std::endl;
      return 0;
    }
    
    resolution = getCloudResolution (cloud);
  }
  else if (!ply_filename_indices.empty ())
  {
    filename = argv[ply_filename_indices[0]];    
    boost::filesystem::path ply_path (filename);
    image_name = ply_path.stem().c_str();
    
    pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh);
    if (pcl::io::loadPLYFile (filename, *mesh) == -1)
    {
      std::cerr << "Was not able to open file '" << filename << "'." << std::endl;
      return 0;
    }
    pcl::fromPCLPointCloud2 (mesh->cloud, *cloud);
    
    resolution = getMeshResolution (mesh);
  }
  else
  {
    std::cerr << "Image not set." << std::endl;
    printUsage (argv[0]);
    return 0;
  }
  
  std::cout << "Loaded " << cloud->size () << " points from file '" << filename << "'." << std::endl;
  std::cout << "Cloud/mesh resolution " << resolution << "." << std::endl;
  
  
  // ---- Extract keypoints ---- //

  std::vector<int> keypoint_indices;
  
  if (use_keypoints)
  {  
    pcl::SBPKeypoint<PointType> sbp_keypoints;
    sbp_keypoints.setInputCloud (cloud);
//    sbp_keypoints.setUseCloudDimensions (use_cloud_dimensions);
    sbp_keypoints.setRadiusSearch (resolution * scale); // If use_cloud_dimensions is set to true, this param will be ignored
    sbp_keypoints.setSelectionMethod (pcl::SBPKeypoint<PointType>::SBP_N, 30); // N30
    
    pcl::PointCloud<int> keypoints_cloud;
    sbp_keypoints.compute (keypoints_cloud);
    
    keypoint_indices.reserve (keypoints_cloud.size ());
    for (pcl::PointCloud<int>::iterator it = keypoints_cloud.begin (); it != keypoints_cloud.end (); ++it)
      keypoint_indices.push_back (*it);
    
    std::cout << "Found " << keypoint_indices.size () << " keypoints." << std::endl;
  }
  
  
  // ---- Compute descriptors ---- //
  
//  pcl::console::setVerbosityLevel (pcl::console::L_DEBUG);
  
  // Create the SBP estimation class and pass the input dataset
  pcl::SBPEstimation<PointType, OutType> sbp;

  sbp.setInputCloud (cloud);
  if (use_keypoints)
    sbp.setIndices (boost::make_shared<std::vector<int> > (keypoint_indices));
  sbp.setUseCloudDimensions (use_cloud_dimensions);
  sbp.setRadiusSearch (resolution * scale); // If use_cloud_dimensions is set to true, this param will be ignored
  
  // Compute the features
  pcl::PointCloud<OutType>::Ptr sbph (new pcl::PointCloud<OutType> ());
  sbp.compute (*sbph);
  
  std::cout << "Generated " << sbph->size () << " SBP descriptors." << std::endl;
  
  
  // ---- Save output ---- //
  
  if (save_output)
  {
    std::string outfile = output + "/" + image_name + "_sbp.pcd";
    pcl::io::savePCDFile (outfile, *sbph, true);
    std::cout << "Features saved in file '" << outfile << "'." << std::endl;
  }
}
