/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
#ifndef PCL_KEYPOINTS_SBP_H_
#define PCL_KEYPOINTS_SBP_H_

#include <pcl/keypoints/keypoint.h>
#include <boost/unordered_map.hpp>
#include <boost/tuple/tuple.hpp>

namespace pcl
{  
	/** \brief @b SBPKeypoint estimates keypoints based binary pattern descriptors for a 
	 * given point cloud.
	 * 
	 * \author Cristina Romero-González
	 * \ingroup keypoints
	 */
	template <typename PointInT>
	class SBPKeypoint: public Keypoint<PointInT, int>
	{
	public:
    
    typedef enum 
    {
      SBP_FREQ = 1,   // Frequency (% of less frequent patterns)
      SBP_NMIN,       // Min number of uniform pattern idx (pattern_idx >= N)
      SBP_N,          // N patterns (N/2 with lowest pattern_idx and N/2 with highest pattern_idx)
      SBP_M           // Max number of points
    } SelectionMethod;
    
		typedef boost::shared_ptr<SBPKeypoint<PointInT> > Ptr;
		typedef boost::shared_ptr<const SBPKeypoint<PointInT> > ConstPtr;

		typedef typename Keypoint<PointInT, int>::PointCloudIn PointCloudIn;
		typedef typename Keypoint<PointInT, int>::PointCloudOut PointCloudOut;
		
		using Keypoint<PointInT, int>::input_;
		using Keypoint<PointInT, int>::indices_;
		
		std::vector<Eigen::Affine3f> transform, inv_transform;
		
		/** \brief Constructor. */
		SBPKeypoint () :
		  bins_ (),
		  bin_indices_ (),
			bin_size_ (Eigen::Vector4f::Zero ()),
			inverse_bin_size_ (Eigen::Vector4f::Zero ()),
			min_b_ (Eigen::Vector4i::Zero ()),
			max_b_ (Eigen::Vector4i::Zero ()),
			div_b_ (Eigen::Vector4i::Zero ()),
			divb_mul_ (Eigen::Vector4i::Zero ()),
			
		  pattern_bins_ (4),
		  pattern_size_ (64),
		  selection_method_ (SBP_FREQ),
			selection_value_ (0),
      bins_pattern_idx_ (),
      patterns_hist_ (),
      selected_patterns_mask_ ()
		{
			name_ = "SBPKeypoint";
		};
		
		/** \brief Destructor */
		~SBPKeypoint ()
		{
		  bins_.clear ();
		  bins_pattern_idx_.clear ();
		  patterns_hist_.clear ();
		  selected_patterns_mask_.clear ();
		};

		/** \brief Set the side size for each squared bin.
		  * \param[in] bin_size bin side size
		  */
		inline void
		setBinSize (float bin_size)
		{
			bin_size_ = Eigen::Vector4f (bin_size, bin_size, bin_size, 1);
			inverse_bin_size_ = Eigen::Array4f::Ones () / bin_size_.array ();
		}
		
		/** \brief Get the side size for each squared bin. */
		inline float 
		getBinSize ()
		{
			return (bin_size_[0]);
		}
		
		/** \brief Set the number of divisions in each dimension of the pattern.
		  * \param[in] pattern_bins number of divisions 
		  */
		inline void
		setPatternBins (int pattern_bins)
		{
		  pattern_size_ = pattern_bins * pattern_bins * pattern_bins;
			pattern_bins_ = pattern_bins;
		}
		
		/** \brief Get the number of divisions in each dimension of the pattern. */
		inline int 
		getPatternBins ()
		{
			return (pattern_bins_);
		}
		
		/** Set the number of bins in the pattern
		  * \param[in] pattern_size number of bins in the pattern
		  */
		inline void
		setPatternSize (int pattern_size)
		{
		  pattern_size_ = pattern_size;
		  pattern_bins_ = static_cast<int> (std::pow(static_cast<float> (pattern_size), 1.0f/3));
		}
		
		/** \brief Get the number of bins in the pattern */
		inline int 
		getPatternSize ()
		{
			return (pattern_size_);
		}
		
		/** \brief Set the selection method
   		* \param[in] selection_method selection method
		  * \param[in] selection_value selection value
		  */
		inline void
		setSelectionMethod (SelectionMethod selection_method, int selection_value)
		{
		  selection_method_ = selection_method;
			selection_value_ = selection_value;
		}
		
		/** \brief Get the histogram of uniform patterns indexes. */
		inline std::vector<int>& 
		getPatternsHist ()
		{
			return (patterns_hist_);
		}
		
		/** \brief Get the selection frequency limit for selected patterns (used with selection criterion SBP_FREQ). */
		inline std::vector<unsigned char>&
		getSelectedPatternsMask ()
		{
			return (selected_patterns_mask_);
		}
		
  protected:
		
		using Keypoint<PointInT, int>::name_;
		using Keypoint<PointInT, int>::surface_;
		using Keypoint<PointInT, int>::tree_;
		using Keypoint<PointInT, int>::k_;
		using Keypoint<PointInT, int>::search_radius_;
		using Keypoint<PointInT, int>::getClassName;
		using Keypoint<PointInT, int>::deinitCompute;
		
		/** \brief The 3D grid for the input cloud. */
		std::vector<unsigned char> bins_;
		
		/** \brief Indices of occupied bins (bin_idx, i, j, k). */
		std::vector<boost::tuple<int, int, int, int> > bin_indices_;
		
		/** \brief The size of a bin. */
		Eigen::Vector4f bin_size_;
		
		/** \brief Internal bin sizes stored as 1/bin_size_ for efficiency reasons. */ 
		Eigen::Array4f inverse_bin_size_;
		
		/** \brief The minimum and maximum bin coordinates, the number of divisions, and the division multiplier. */
		Eigen::Vector4i min_b_, max_b_, div_b_, divb_mul_;
		
		/** \brief The minimum and maximum bounding box coordinates. */
		Eigen::Vector4f min_p_, max_p_;
		
		/** \brief The uniform pattern idx associated to each bin. */
		std::vector<int> bins_pattern_idx_;
		
		/** \brief The histogram of uniform patterns. */
		std::vector<int> patterns_hist_;
		
		/** \brief The selected patterns mask. */
		std::vector<unsigned char> selected_patterns_mask_;
		
		/** \brief The number of divisions in each dimension of the pattern. */
		int pattern_bins_;
		
		/** \brief The number of bins in the pattern. */
		int pattern_size_;
		
    /** \brief Padding in pattern from center bin. */
		int pattern_left_padding_, pattern_right_padding_;
		
		/** \brief Selection method */
		SelectionMethod selection_method_;
		
		/** \brief Selection value for selected patterns with selection method (range will depend on selection method). */
		int selection_value_;
		
		
		/** \brief This method should get called before starting the actual computation. */
		virtual bool
		initCompute ();
		
		/** \brief Estimate the Spherical Binary Pattern (SBP) descriptors at a set of points given by
		 * <setInputCloud (), setIndices ()> using the surface in setSearchSurface () and the spatial locator in
		 * setSearchMethod ()
		 * \param[out] output the resultant point cloud model dataset that contains the SBP feature estimates
		 */
		void 
		detectKeypoints (PointCloudOut &output);
		
		void
		calculateOccupation3DGrid ();
		
		void
		analyzeUniformPatterns ();
		
		void
		calculateSelectedPatterns ();
		
    int
		getPatternConnectedComponents (int i_center, int j_center, int k_center, Eigen::VectorXi &pattern_components);
		
	private:
		
		void
		getPattern (int i, int j, int k, Eigen::VectorXi &pattern);
		
		inline bool
		sortCompare (int i1, int i2)
		{
			return patterns_hist_[i1] < patterns_hist_[i2];
		}
	};
}
					  
#ifdef PCL_NO_PRECOMPILE
#include "pcl/keypoints/impl/sbp_keypoint.hpp"
#endif

#endif  //#ifndef PCL_KEYPOINTS_SBP_H_
