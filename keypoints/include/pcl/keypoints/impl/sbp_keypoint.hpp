/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
#ifndef PCL_KEYPOINTS_SBP_IMPL_H_
#define PCL_KEYPOINTS_SBP_IMPL_H_

#include <cmath>
#include <queue>

#include <pcl/common/common.h>

#include "pcl/keypoints/sbp_keypoint.h"

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> void
pcl::SBPKeypoint<PointInT>::detectKeypoints (PointCloudOut &output)
{
	output.height = 1; // downsampling breaks the organized structure
	output.is_dense = true; // we filter out invalid points

  // Calculate occupation binary 3D grid based on input cloud points position.
  calculateOccupation3DGrid ();
  
  // Analyze uniform patterns distribution
  patterns_hist_.clear ();
  analyzeUniformPatterns ();
//  for (int i = 0; i < patterns_hist_.size (); i++)
//  	std::cout << patterns_hist_[i] << " ";
//  std::cout << std::endl;
  
  // Calculate selection mask
  selected_patterns_mask_.clear ();
  calculateSelectedPatterns ();
  
	// Search closest point for selected patterns
	Eigen::Vector4f bin_center;
	if (pattern_bins_ % 2 == 0)
	  bin_center = bin_size_.array ();
	else
    bin_center = bin_size_.array () / 2.0f;
	bin_center[3] = 0;
	
	for (size_t idx = 0; idx < bins_pattern_idx_.size (); ++idx)
  {
  	if (bins_pattern_idx_[idx] >= 0 && selected_patterns_mask_[bins_pattern_idx_[idx]])
  	{
  	  int bin_idx, i, j, k;
      boost::tie (bin_idx, i, j, k) = bin_indices_[idx];
      Eigen::Vector4i ijk (i, j, k, 1);
  	  
  	  std::vector<int> indices (1);
      std::vector<float> sqr_distances (1);
      
      PointInT p;
      p.getVector4fMap () = ijk.cast<float> ().cwiseProduct(bin_size_) + bin_center; // + min_p_;
      
      int nres = tree_->nearestKSearch (p, 1, indices, sqr_distances);
      if (nres == 1)
      {
        output.points.push_back (indices[0]);
      }
    }
  }
	
	output.width = static_cast<uint32_t> (output.points.size ());
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> void
pcl::SBPKeypoint<PointInT>::calculateOccupation3DGrid ()
{
  // Get the minimum and maximum dimensions
	pcl::getMinMax3D<PointInT>(*surface_, min_p_, max_p_);
  
  // Calculate pattern padding from center bin
  pattern_left_padding_ = (pattern_bins_ % 2 == 0) ? pattern_bins_ / 2 - 1 : std::floor (pattern_bins_ / 2);
  pattern_right_padding_ = (pattern_bins_ % 2 == 0) ? pattern_bins_ / 2 : std::floor (pattern_bins_ / 2);
  
  // Add padding to cloud
  min_p_ -= (bin_size_ * pattern_left_padding_);
  max_p_ += (bin_size_ * pattern_right_padding_);
  min_p_[3] = 1;
  max_p_[3] = 1;
  
  // Compute the minimum and maximum bounding box values
	min_b_ = (min_p_.array() * inverse_bin_size_).unaryExpr(std::ptr_fun(floor)).template cast<int>();
	max_b_ = (max_p_.array() * inverse_bin_size_).unaryExpr(std::ptr_fun(floor)).template cast<int>(); // ceil?
	
	// Compute the number of divisions needed along all axis
	div_b_ = max_b_ - min_b_ + Eigen::Vector4i::Ones ();
	div_b_[3] = 0;
	
	// Set up the division multiplier
	divb_mul_ = Eigen::Vector4i (1, div_b_[0], div_b_[0] * div_b_[1], 0);
	
	// Initialize 3d grid
	bins_.clear ();
	bins_.resize (div_b_[0] * div_b_[1] * div_b_[2], 0);
	bin_indices_.resize (bins_.size ());
	int n_bins = 0;
	
	// Build the grid of occupied bins
	for (size_t cp = 0; cp < indices_->size (); ++cp)
	{
		if (!input_->is_dense)
			if (!pcl_isfinite (input_->points[(*indices_)[cp]].x) || 
				!pcl_isfinite (input_->points[(*indices_)[cp]].y) || 
				!pcl_isfinite (input_->points[(*indices_)[cp]].z))
				continue;   // Invalid point
		
		Eigen::Vector4i ijk = Eigen::Vector4i::Zero ();
		ijk[0] = static_cast<int> (floor (input_->points[(*indices_)[cp]].x * inverse_bin_size_[0]));
		ijk[1] = static_cast<int> (floor (input_->points[(*indices_)[cp]].y * inverse_bin_size_[1]));
		ijk[2] = static_cast<int> (floor (input_->points[(*indices_)[cp]].z * inverse_bin_size_[2]));
		
		// Compute the bin index
		int idx = (ijk - min_b_).dot (divb_mul_);
		
		if (bins_[idx] == 0)
		{
			bins_[idx] = 1;
			bin_indices_[n_bins] = boost::make_tuple (idx, ijk[0], ijk[1], ijk[2]);
			n_bins++;
		}
	}
	
	bin_indices_.resize (n_bins);
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> void
pcl::SBPKeypoint<PointInT>::analyzeUniformPatterns ()
{
  // Initialize patterns params
  bins_pattern_idx_.resize (bin_indices_.size());   // Patterns can only be associated to occupied bins
  patterns_hist_.resize (pattern_size_ + 2, 0);     // Patterns histogram: 0-pattern_size_ + Non-uniform
  
  // Iterate over the occupied bins in the 3D grid and calculate associated pattern idx
  for (int idx = 0; idx < bin_indices_.size (); idx++)
  {
    int bin_idx, i, j, k;
    boost::tie (bin_idx, i, j, k) = bin_indices_[idx];
    
    // Find connected componets
    Eigen::VectorXi pattern_components = Eigen::VectorXi::Zero (pattern_size_);
    int n_components = getPatternConnectedComponents (i, j, k, pattern_components);
    
    // Assign pattern idx
    int pattern_idx = 0;
    int non_uniform_idx = pattern_size_ + 1;
    if (n_components > 0)
      pattern_idx = (n_components == 1) ? pattern_components.sum () : non_uniform_idx;
    
    // Update accumulated info
    patterns_hist_[pattern_idx]++;
    bins_pattern_idx_[idx] = pattern_idx;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> void
pcl::SBPKeypoint<PointInT>::calculateSelectedPatterns () 
{
  // Initialize selection mask
  selected_patterns_mask_.resize (patterns_hist_.size (), 0);
  
  switch (selection_method_)
  {
    case SBP_FREQ:
      {
        // http://stackoverflow.com/a/12399290
        // Initialize original index locations
        std::vector<int> idx (patterns_hist_.size ());
        for (int i = 0; i != idx.size (); ++i) idx[i] = i;
      
        // Sort indexes by histogram value (patterns_hist_)
        std::sort(idx.begin(), idx.end(), boost::bind( &pcl::SBPKeypoint<PointInT>::sortCompare, this, _1, _2 ));
        
        // Select patterns
        int nr_selected_patterns = 0;
        for (int i = 0; i < idx.size (); i++)
        {
          if (patterns_hist_[idx[i]] > 0 && idx[i] != pattern_size_ + 1 && idx[i] != 0)
          {
	          selected_patterns_mask_[idx[i]] = 1;
	          nr_selected_patterns++;
	        }
	        
      	  if (nr_selected_patterns == selection_value_)
	      	  break;
      	}
    	}
    	
      break;
      
    case SBP_NMIN:
      {
        for (int i = selection_value_; i <= pattern_size_; i++)
		      selected_patterns_mask_[i] = 1;
		  }
		    
      break;
      
    case SBP_N:
      {
        int half_n = selection_value_ / 2;
        
        for (int i = 1; i <= half_n; i++)
		      selected_patterns_mask_[i] = 1;
		    
		    for (int i = pattern_size_ - half_n + 1; i <= pattern_size_; i++)
		      selected_patterns_mask_[i] = 1;
		  }
		  
      break;
      
    case SBP_M:
      {
        // http://stackoverflow.com/a/12399290
        // Initialize original index locations
        std::vector<int> idx (patterns_hist_.size ());
        for (int i = 0; i != idx.size (); ++i) idx[i] = i;
      
        // Sort indexes based on comparing values in patterns_hist_
        std::sort(idx.begin(), idx.end(), boost::bind( &pcl::SBPKeypoint<PointInT>::sortCompare, this, _1, _2 ));
      
        // Select patterns
        int nr_selected_points = 0;
        for (int i = 0; i < idx.size (); i++)
        {
          if (patterns_hist_[idx[i]] > 0 && idx[i] != pattern_size_ + 1 && idx[i] != 0)
          {
            if ((nr_selected_points + patterns_hist_[idx[i]]) > selection_value_)
	      	    break;
	      	  
	          selected_patterns_mask_[idx[i]] = 1;
	          nr_selected_points += patterns_hist_[idx[i]];
	        }
      	}
    	}
    	
      break;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> int
pcl::SBPKeypoint<PointInT>::getPatternConnectedComponents (int i_center, int j_center, int k_center, Eigen::VectorXi &pattern_components)
{
  std::vector<std::vector<std::vector<int> > > components_grid (pattern_bins_, std::vector<std::vector<int> > (pattern_bins_, std::vector<int> (pattern_bins_, -1)));
  
  // https://en.wikipedia.org/wiki/Connected-component_labeling
  int current_id = 1;
  for (int i = i_center - pattern_left_padding_; i <= (i_center + pattern_right_padding_); i++)
  {
    for (int j = j_center - pattern_left_padding_; j <= (j_center + pattern_right_padding_); j++)
    {
      for (int k = k_center - pattern_left_padding_; k <= (k_center + pattern_right_padding_); k++)
      {
        Eigen::Vector4i ijk (i, j, k, 1);
        int bin_idx = (ijk - min_b_).dot (divb_mul_); 
        int i_pattern = i - i_center + pattern_left_padding_;
        int j_pattern = j - j_center + pattern_left_padding_;
        int k_pattern = k - k_center + pattern_left_padding_;
        
        if (components_grid[i_pattern][j_pattern][k_pattern] >= 0)
          continue;
          
        if (bins_[bin_idx] == 0)
        {
          components_grid[i_pattern][j_pattern][k_pattern] = 0;
          continue;
        }
        
        components_grid[i_pattern][j_pattern][k_pattern] = current_id;
        
        std::queue<boost::tuple<int, int, int> > queue; // BFS (6-connectivity)
        queue.push (boost::make_tuple (i, j, k));
        
        while (!queue.empty ())
        {
          int i2, j2, k2;
          boost::tie (i2, j2, k2) = queue.front ();
          queue.pop ();
      
          for (int pos = 0; pos < 6; pos++)
          {
            int i3 = i2, j3 = j2, k3 = k2;
            switch (pos)
            {
              case 0: // Left
                i3--;
                break;
              case 1: // Right
                i3++;
                break;
              case 2: // Bottom
                j3--;
                break;
              case 3: // Top
                j3++;
                break;
              case 4: // Front
                k3--;
                break;
              case 5: // Back
                k3++;
                break;
            }
            
            Eigen::Vector4i ijk3 (i3, j3, k3, 1);
            int bin_idx3 = (ijk3 - min_b_).dot (divb_mul_);
            int i3_pattern = i3 - i_center + pattern_left_padding_;
            int j3_pattern = j3 - j_center + pattern_left_padding_;
            int k3_pattern = k3 - k_center + pattern_left_padding_;
            
            if (i3_pattern < 0 || i3_pattern >= pattern_bins_ ||
                j3_pattern < 0 || j3_pattern >= pattern_bins_ ||
                k3_pattern < 0 || k3_pattern >= pattern_bins_)
              continue;
            
            if (components_grid[i3_pattern][j3_pattern][k3_pattern] < 0)
            {
              if (bins_[bin_idx3] == 1)
              {
                components_grid[i3_pattern][j3_pattern][k3_pattern] = current_id;
                queue.push (boost::make_tuple (i3, j3, k3));
              }
              else
                components_grid[i3_pattern][j3_pattern][k3_pattern] = 0;
            }
          }
        }
        
        current_id++;
      }
    }
  }
  
  // Copy pattern
  int id = 0;
  for (int i_pattern = 0; i_pattern < pattern_bins_; i_pattern++)
    for (int j_pattern = 0; j_pattern < pattern_bins_; j_pattern++)
      for (int k_pattern = 0; k_pattern < pattern_bins_; k_pattern++)
        pattern_components[id++] = components_grid[i_pattern][j_pattern][k_pattern];
    
  return current_id - 1;
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> void
pcl::SBPKeypoint<PointInT>::getPattern (int i, int j, int k, Eigen::VectorXi &pattern)
{
  int id = 0;
  for (int i2 = i - pattern_left_padding_; i2 <= (i + pattern_right_padding_); i2++)
  {
    for (int j2 = j - pattern_left_padding_; j2 <= (j + pattern_right_padding_); j2++)
    {
      for (int k2 = k - pattern_left_padding_; k2 <= (k + pattern_right_padding_); k2++)
      {
        Eigen::Vector4i ijk (i2, j2, k2, 1);
        int bin_id = (ijk - min_b_).dot (divb_mul_);
        pattern[id++] = bins_[bin_id];
      }
    }
  }
  
  if (id != pattern_size_)
    PCL_ERROR ("[pcl::%s::getPattern] Wrong pattern size for bin %d,%d,%d. Generated pattern of size %d, expected %d\n", getClassName ().c_str (), i, j, k, id, pattern_size_);
    
  return;
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT> bool
pcl::SBPKeypoint<PointInT>::initCompute ()
{
  // Calculate bin size from search radius
  if (search_radius_)
  {
    float bin_size = 2.0f * search_radius_ / (pattern_bins_ * std::sqrt (3.0f));
    setBinSize (bin_size);
    search_radius_ = 0;
  }
  k_ = 1; // We will only search for the closest point to the pattern center
  
  if (!Keypoint<PointInT, int>::initCompute ())
  {
    PCL_ERROR ("[pcl::%s::initCompute] Init failed.\n", getClassName ().c_str ());
    return (false);
  }
	
	// Validate selection method/value
	if (!selection_value_)
	{
		PCL_ERROR ("[pcl::%s::initCompute] Selection value not set.\n", getClassName ().c_str ());
    return (false);
	}
	
	switch (selection_method_)
	{
	  case SBP_FREQ:
	    if (selection_value_ > 100 || selection_value_ <= 0)
	    {
	      PCL_ERROR ("[pcl::%s::initCompute] Selection value '%d' not valid for current selection method (SBP_FREQ), please set value in range 1:100.\n", getClassName ().c_str (), selection_value_);
	      return (false);
	    }
	    break;
	  case SBP_NMIN:
	  case SBP_N:
	    if (selection_value_ > pattern_size_ || selection_value_ <= 0)
	    {
	      PCL_ERROR ("[pcl::%s::initCompute] Selection value '%d' not valid for current selection method (SBP_NMIN or SBP_N), please set value in range 1-%d.\n", getClassName ().c_str (), selection_value_, pattern_size_);
	      return (false);
	    }
	    break;
	  case SBP_M:
	    // No validation required
	    break;
	}
  
  // Validate bin size
  if (!getBinSize ())
  {
    PCL_ERROR ("[pcl::%s::initCompute] Bin size not set. Please, use 'setBinSize' or 'setRadiusSearch'\n", getClassName ().c_str ());
    return (false);
  }
    
  return (true);
}


#define PCL_INSTANTIATE_SBPKeypoint(T) template class PCL_EXPORTS pcl::SBPKeypoint<T>;
#endif    // PCL_KEYPOINTS_SBP_IMPL_H_
