set(MODULE_NAME keypoints)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")

set(HEADERS
  include/pcl/${MODULE_NAME}/sbp_keypoint.h
)

set(IMPL_HEADERS
  include/pcl/${MODULE_NAME}/impl/sbp_keypoint.hpp
)

set(SOURCE
  src/sbp_keypoint.cpp
)

if(SOURCE)
  add_library("sbp_${MODULE_NAME}" SHARED ${HEADERS} ${IMPL_HEADERS} ${SOURCE})

  install(TARGETS "sbp_${MODULE_NAME}" EXPORT sbp
    DESTINATION ${LIB_INSTALL_DIR} COMPONENT "sbp_${MODULE_NAME}" EXPORT sbp)
endif(SOURCE)

install(FILES ${HEADERS}
  DESTINATION "${INCLUDE_INSTALL_DIR}/pcl/${MODULE_NAME}" COMPONENT "sbp_${MODULE_NAME}")
  
install(FILES ${IMPL_HEADERS}
  DESTINATION "${INCLUDE_INSTALL_DIR}/pcl/${MODULE_NAME}/impl" COMPONENT "sbp_${MODULE_NAME}")
