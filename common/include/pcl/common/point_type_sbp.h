/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PCL_POINT_SBP_H_
#define PCL_POINT_SBP_H_

namespace pcl
{
//  struct SBPSignature
//  {
//    int pattern[256];
//    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
//  } EIGEN_ALIGN16;                     // enforce SSE padding for correct memory alignment
  
  struct SBPSignature64
  {
    PCL_ADD_POINT4D;	// preferred way of adding a XYZ+padding
    unsigned char pattern[64];
    float rf[9];
    static int descriptorSize () { return 64; }
    
    friend std::ostream& operator << (std::ostream& os, const SBPSignature64& p);
    
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
  } EIGEN_ALIGN16;
  
  inline std::ostream& 
  operator << (std::ostream& os, const SBPSignature64& p)
  {
    os << "(" << p.x << "," << p.y << "," << p.z << ") - ";
    for (int i = 0; i < 9; ++i)
      os << (i == 0 ? "(" : "") << p.rf[i] << (i < 8 ? ", " : ")");
    for (int i = 0; i < 64; ++i)
	    os << (i == 0 ? "(" : "") << static_cast<unsigned> (p.pattern[i]) << (i < 63 ? ", " : ")");
    return (os);
  }
  
  struct SBPSignature27
  {
    PCL_ADD_POINT4D;	// preferred way of adding a XYZ+padding
    unsigned char pattern[27];
    float rf[9];
    static int descriptorSize () { return 27; }
    
    friend std::ostream& operator << (std::ostream& os, const SBPSignature27& p);
    
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
  } EIGEN_ALIGN16;
  
  inline std::ostream& 
  operator << (std::ostream& os, const SBPSignature27& p)
  {
    os << "(" << p.x << "," << p.y << "," << p.z << ") - ";
    for (int i = 0; i < 9; ++i)
      os << (i == 0 ? "(" : "") << p.rf[i] << (i < 8 ? ", " : ")");
    for (int i = 0; i < 27; ++i)
	    os << (i == 0 ? "(" : "") << static_cast<unsigned> (p.pattern[i]) << (i < 26 ? ", " : ")");
    return (os);
  }
}

POINT_CLOUD_REGISTER_POINT_STRUCT (pcl::SBPSignature64,
								  (float, x, x)
								  (float, y, y)
								  (float, z, z)
								  (float[9], rf, rf)
  								(unsigned char[64], pattern, sbp)
)

POINT_CLOUD_REGISTER_POINT_STRUCT (pcl::SBPSignature27,
								  (float, x, x)
								  (float, y, y)
								  (float, z, z)
								  (float[9], rf, rf)
  								(unsigned char[27], pattern, sbp)
)

#endif  //#ifndef PCL_POINT_SBP_H_
