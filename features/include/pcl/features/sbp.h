/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PCL_SBP_H_
#define PCL_SBP_H_

#include <pcl/point_types.h>
#include <pcl/features/feature.h>

#include "pcl/common/point_type_sbp.h"

namespace pcl
{
	/** \brief @b SBPEstimation estimates binary pattern descriptors for a given point cloud
	 * dataset containing points.
	 * 
	 * \author Cristina Romero-González
	 * \ingroup features
	 */
	template <typename PointInT, typename PointOutT>
	class SBPEstimation: public Feature<PointInT, PointOutT>
	{
	public:
		typedef boost::shared_ptr<SBPEstimation<PointInT, PointOutT> > Ptr;
		typedef boost::shared_ptr<const SBPEstimation<PointInT, PointOutT> > ConstPtr;
		
		typedef typename pcl::ReferenceFrame PointRF;
		
		using Feature<PointInT, PointOutT>::indices_;
		using Feature<PointInT, PointOutT>::input_;

		typedef typename pcl::PointCloud<pcl::PointNormal> PointCloudN;
		typedef typename PointCloudN::Ptr PointCloudNPtr;
		typedef typename PointCloudN::ConstPtr PointCloudNConstPtr;

		typedef typename Feature<PointInT, PointOutT>::PointCloudIn PointCloudIn;
		typedef typename Feature<PointInT, PointOutT>::PointCloudInPtr PointCloudInPtr;
		typedef typename Feature<PointInT, PointOutT>::PointCloudOut PointCloudOut;
		typedef typename PointCloudOut::Ptr PointCloudOutPtr;
		
		/** \brief Constructor. */
		SBPEstimation () :
		  bin_size_ (0),
		  cloud_resolution_ (0),
		  use_cloud_resolution_ (false),
		  use_cloud_dimensions_ (false)
		{
			feature_name_ = "SBPEstimation";
			
			total_bins_ = PointOutT::descriptorSize ();
			nr_bins_ = static_cast<int> (std::pow(static_cast<float> (total_bins_), 1.0f/3));
			bin_mult_ = Eigen::Vector4i (1, nr_bins_, nr_bins_ * nr_bins_, 0);
			
			PCL_DEBUG ("[pcl::%s] Patterns of size %d (%d, %d, %d)\n", getClassName ().c_str (), total_bins_, nr_bins_, nr_bins_, nr_bins_);
		};
		
		/** \brief Empty destructor */
//		~SBPEstimation () { };
		
		/** \brief Get the number of divisions in each dimension. */
		inline int 
		getNrBins ()
		{
			return (nr_bins_);
		}

		/** \brief Set the side size for each squared bin.
		  * \param[in] bin_size bin side size
		  */
		inline void
		setBinSize (float bin_size)
		{
			bin_size_ = bin_size;
			search_radius_ = bin_size * nr_bins_ * std::sqrt (3.0f) / 2;
		}
		
		/** \brief Get the side size for each squared bin. */
		inline float 
		getBinSize ()
		{
			return (bin_size_);
		}
		
		/** \brief Get the cloud resolution. */
		inline double 
		getCloudResolution ()
		{
			return (cloud_resolution_);
		}
		
		/** \brief Set search radius. */
		inline void 
		setRadiusSearch (double radius)
		{
		  search_radius_ = radius;
		  bin_size_ = 2.0f * radius / (nr_bins_ * std::sqrt(3.0f));
		}
		
		/** \brief Set the use of cloud dimensions to calculate the bin size (WACV 2016). 
		  * \param[in] use_cloud_dimensions 
		  */
		inline void
		setUseCloudDimensions (bool use_cloud_dimensions)
		{
			use_cloud_dimensions_ = use_cloud_dimensions;
		}
		
		/** \brief Get the number of bins in the descriptor. */
		inline int
		getTotalBins ()
		{
			return (total_bins_);
		}
		
  protected:
		
		using Feature<PointInT, PointOutT>::feature_name_;
		using Feature<PointInT, PointOutT>::surface_;
		using Feature<PointInT, PointOutT>::fake_surface_;
		using Feature<PointInT, PointOutT>::tree_;
		using Feature<PointInT, PointOutT>::search_radius_;
		using Feature<PointInT, PointOutT>::k_;
		using Feature<PointInT, PointOutT>::search_parameter_;
		using Feature<PointInT, PointOutT>::getClassName;
		using Feature<PointInT, PointOutT>::deinitCompute;
		
		/** \brief The number of divisions in each dimension. */
		int nr_bins_;
		
		/** \brief Side size for each squared bin. */
		float bin_size_;
		
		/** \brief Cloud resolution. */
		double cloud_resolution_;
		
		/** \brief Use cloud resolution for dynamic bin size calculation. */
		bool use_cloud_resolution_;
		
		/** \brief Use cloud dimensions for dynamic bin size calculation. */
		bool use_cloud_dimensions_;
		
		/** \brief This method should get called before starting the actual computation. */
		virtual bool
		initCompute ();
      
		/** \brief Estimate the Spherical Binary Pattern (SBP) descriptors at a set of points given by
		 * <setInputCloud (), setIndices ()> using the surface in setSearchSurface () and the spatial locator in
		 * setSearchMethod ()
		 * \param[out] output the resultant point cloud model dataset that contains the SBP feature estimates
		 */
		void 
		computeFeature (PointCloudOut &output);
		
		/** \brief Estimate the invariant Local Reference Frame for a given 3D point
		  * \param[in] index the index of the point in indices_
		  * \param[in] nn_indices the indices of the points in the local neighborhood
		  * \param[out] rf the local reference frame at the query point
		  */
		bool
		getLocalRF (const int index, const std::vector<int> nn_indices, PointRF &output_rf);
	
	private:
		/** \brief Inverse of the bin size. */
		float inverse_bin_size_;
		
		/** \brief Multiplier to calculate bin index. */
		Eigen::Vector4i bin_mult_;
		
		/** \brief Grid bounding box. */
		Eigen::Vector4f min_b_, max_b_;
		
		/** \brief Total number of bins == Pattern size. */
		int total_bins_;
        
		/** \brief Estimate the SBP (Spherical Binary Pattern) individual signature for a given 3D point
		  * \param[in] index the index of the point in indices_
		  * \param[in] nn_indices the indices of the points in the local neighborhood
		  * \param[in] rf local reference frame
		  * \param[out] hist the resultant SBP histogram representing the feature at the query point
		  */
		void
		computePointSBP (const int index, const std::vector<int> nn_indices, const PointRF &rf, Eigen::VectorXf &hist);
	};
}
					  
#ifdef PCL_NO_PRECOMPILE
#include "pcl/features/impl/sbp.hpp"
#endif

#endif  //#ifndef PCL_SBP_H_
