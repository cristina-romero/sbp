/*
 * Software License Agreement (BSD License)
 * 
 * Copyright (c) 2015, Cristina Romero-González (UCLM)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are 
 * permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be 
 * used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PCL_FEATURES_IMPL_SBP_H_
#define PCL_FEATURES_IMPL_SBP_H_

#include <cmath>

#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/common/transformation_from_correspondences.h>
#include <pcl/io/pcd_io.h>

#include "pcl/features/sbp.h"

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT,  typename PointOutT> void
pcl::SBPEstimation<PointInT, PointOutT>::computeFeature (PointCloudOut &output)
{
   tree_->setSortedResults (true); // For RF calculation
   
  std::vector<int> nn_indices; // (k_);
  std::vector<float> nn_dists; // (k_);

  // Allocate enough space to hold the results
  output.resize (indices_->size ());
  output.is_dense = true;
  // Save a few cycles by not checking every point for NaN/Inf values if the cloud is set to dense
  if (input_->is_dense)
  {
    // Iterating over the entire index vector
    for (size_t idx = 0; idx < indices_->size (); ++idx)
    {
      output.points[idx].x = (*input_)[(*indices_)[idx]].x;
      output.points[idx].y = (*input_)[(*indices_)[idx]].y;
      output.points[idx].z = (*input_)[(*indices_)[idx]].z;
      
      PointRF current_frame;
      if (this->searchForNeighbors ((*indices_)[idx], search_radius_, nn_indices, nn_dists) == 0 ||
          !getLocalRF (static_cast<int> (idx), nn_indices, current_frame) ||
          !pcl_isfinite (current_frame.x_axis[0]) || /*!pcl_isfinite (current_frame.y_axis[0]) || */!pcl_isfinite (current_frame.z_axis[0]))
      {
        PCL_WARN ("[pcl::%s::computeFeature] Invalid point %d.\n", getClassName ().c_str (), (*indices_)[idx]);
        
        for (int d = 0; d < total_bins_; ++d)
          output.points[idx].pattern[d] = std::numeric_limits<unsigned char>::quiet_NaN ();
				
        output.is_dense = false;
        continue;
      }
      
      Eigen::VectorXf hist;
      computePointSBP (static_cast<int> (idx), nn_indices, current_frame, hist);

      // Copy into the resultant cloud
      for (int i = 0; i < 9; i++)
        output.points[idx].rf[i] = current_frame.rf[i];
      for (int d = 0; d < hist.size (); ++d)
        output.points[idx].pattern[d] = hist[d];
    }
  }
  else
  {
    // Iterating over the entire index vector
    for (size_t idx = 0; idx < indices_->size (); ++idx)
    {
      output.points[idx].x = (*input_)[(*indices_)[idx]].x;
      output.points[idx].y = (*input_)[(*indices_)[idx]].y;
      output.points[idx].z = (*input_)[(*indices_)[idx]].z;
      
      PointRF current_frame;
      if (!pcl::isFinite ((*input_)[(*indices_)[idx]]) ||
          this->searchForNeighbors ((*indices_)[idx], search_radius_, nn_indices, nn_dists) == 0 ||
          !getLocalRF (static_cast<int> (idx), nn_indices, current_frame) ||
          !pcl_isfinite (current_frame.x_axis[0]) || /*!pcl_isfinite (current_frame.y_axis[0]) || */!pcl_isfinite (current_frame.z_axis[0]))
      {
        PCL_WARN ("[pcl::%s::computeFeature] Invalid point %d.\n", getClassName ().c_str (), (*indices_)[idx]);

        for (int d = 0; d < total_bins_; ++d)
          output.points[idx].pattern[d] = std::numeric_limits<unsigned char>::quiet_NaN ();

        output.is_dense = false;
        continue;
      }
      
      Eigen::VectorXf hist;
      computePointSBP (static_cast<int> (idx), nn_indices, current_frame, hist);

      // Copy into the resultant cloud
      for (int i = 0; i < 9; i++)
        output.points[idx].rf[i] = current_frame.rf[i];
      for (int d = 0; d < hist.size (); ++d)
        output.points[idx].pattern[d] = hist[d];

    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT, typename PointOutT> bool
pcl::SBPEstimation<PointInT, PointOutT>::getLocalRF (const int index, const std::vector<int> nn_indices, PointRF &output_rf)
{
  if (!pcl::isFinite ((*input_)[(*indices_)[index]]))
  {
    return (false);
  }
  
  // Covariance Matrix Calculation
  const Eigen::Vector4f& central_point = (*input_)[(*indices_)[index]].getVector4fMap ();
  
  // Placeholder for the 3x3 covariance matrix at each surface patch
  EIGEN_ALIGN16 Eigen::Matrix3f covariance_matrix;
  // 16-bytes aligned placeholder for the XYZ centroid of a surface patch
  Eigen::Vector4f xyz_centroid;
  
  if (nn_indices.size () < 5 ||
      computeMeanAndCovarianceMatrix (*surface_, nn_indices, covariance_matrix, xyz_centroid) == 0)
  {
    return (false);
  }
  
  // Eigenvalues Decomposition
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> solver (covariance_matrix);

  const float& ev1 = solver.eigenvalues ()[0];
  const float& ev2 = solver.eigenvalues ()[1];
  const float& ev3 = solver.eigenvalues ()[2];

  if (!pcl_isfinite (ev1) || !pcl_isfinite (ev2) || !pcl_isfinite (ev3))
  {
    PCL_WARN ("[pcl::%s::getLocalRF] Warning! Eigenvectors are NaN. Aborting Local RF computation of feature point (%lf, %lf, %lf)\n", getClassName ().c_str (), central_point[0], central_point[1], central_point[2]);

    return (false);
  }
  
  // Disambiguation
  Eigen::Vector4f v1 = Eigen::Vector4f::Zero ();
  Eigen::Vector4f v2 = Eigen::Vector4f::Zero ();
  Eigen::Vector4f v3 = Eigen::Vector4f::Zero ();
  v1.head<3> ().matrix () = solver.eigenvectors ().col (2);
//  v2.head<3> ().matrix () = solver.eigenvectors ().col (1);
  v3.head<3> ().matrix () = solver.eigenvectors ().col (0);

  // Diff Matrix
  Eigen::Matrix<float, Eigen::Dynamic, 4> diff_matrix (nn_indices.size (), 4);
  int nr_valid_points = 0;
  for (int i = 0; i < nn_indices.size (); i++)
  {
    Eigen::Vector4f pt = surface_->points[nn_indices[i]].getVector4fMap ();
    if (pt.head<3> () == central_point.head<3> ())
      continue;
    
    diff_matrix.row (nr_valid_points++).matrix () = pt - xyz_centroid;
  }
  
  int plusv1 = 0, /*plusv2 = 0, */plusv3 = 0;
  for (int i = 0; i < nr_valid_points; i++)
  {
    float dp = diff_matrix.row (i).dot (v1);
    if (dp >= 0)
      plusv1++;

//    dp = diff_matrix.row (i).dot (v2);
//    if (dp >= 0)
//      plusv2++;
      
    dp = diff_matrix.row (i).dot (v3);
    if (dp >= 0)
      plusv3++;
  }

  int points = 5; //std::min(valid_nn_points*2/2+1, 11);
  int medianIndex = nr_valid_points / 2;
  
  // v1
  plusv1 = 2 * plusv1 - nr_valid_points;
  if (plusv1 == 0)
  {
    for (int i = -points / 2; i <= points / 2; i++)
      if (diff_matrix.row (medianIndex - i).dot (v1) > 0)
        plusv1++;

    if (plusv1 < points / 2 + 1)
      v1 *= -1;
  } 
  else if (plusv1 < 0)
    v1 *= -1;
    
  // v2
/*  plusv2 = 2 * plusv2 - nr_valid_points;
  if (plusv2 == 0)
  {
    for (int i = -points / 2; i <= points / 2; i++)
      if (diff_matrix.row (medianIndex - i).dot (v2) > 0)
        plusv2++;

    if (plusv2 < points / 2 + 1)
      v2 *= -1;
  } 
  else if (plusv2 < 0)
    v2 *= -1;*/
    
  // v3
  plusv3 = 2 * plusv3 - nr_valid_points;
  if (plusv3 == 0)
  {
    for (int i = -points / 2; i <= points / 2; i++)
      if (diff_matrix.row (medianIndex - i).dot (v3) > 0)
        plusv3++;

    if (plusv3 < points / 2 + 1)
      v3 *= -1;
  } 
  else if (plusv3 < 0)
    v3 *= -1;

  Eigen::Matrix3f rf;
  rf.row (0).matrix () = v1.head<3> ();
  rf.row (1).matrix () = v2.head<3> ();
  rf.row (2).matrix () = v3.head<3> ();
  
  for (int d = 0; d < 3; ++d)
  {
    output_rf.x_axis[d] = rf.row (0)[d];
    output_rf.y_axis[d] = rf.row (1)[d];
    output_rf.z_axis[d] = rf.row (2)[d];
  }
    
  return (true);
}


//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT, typename PointOutT> void
pcl::SBPEstimation<PointInT, PointOutT>::computePointSBP (const int index, const std::vector<int> nn_indices, const PointRF &rf, Eigen::VectorXf &pattern)
{
  PointInT point = (*input_)[(*indices_)[index]];
  
  // Calculate transformation
  pcl::TransformationFromCorrespondences trans;
  trans.add (point.getVector3fMap (), Eigen::Vector3f (0, 0, 0));
  trans.add (point.getVector3fMap () + Eigen::Vector3f::Map (rf.x_axis), Eigen::Vector3f (1, 0, 0));
  trans.add (point.getVector3fMap () + Eigen::Vector3f::Map (rf.z_axis), Eigen::Vector3f (0, 0, 1));
  Eigen::Affine3f transformation = trans.getTransformation ();

  // Filter points
  pattern = Eigen::VectorXf::Zero(total_bins_);
  PointInT point_t = pcl::transformPoint<PointInT> (point, transformation);
  
  for (size_t idx = 0; idx < nn_indices.size (); ++idx)
  {
    PointInT p = pcl::transformPoint<PointInT> (surface_->points[nn_indices[idx]], transformation);
    
    // If inside the grid and not the center point
    if (pcl::isFinite (p)) {
      Eigen::Vector4f p_vector = p.getVector4fMap ();
      
      if (point_t.getVector3fMap () != p_vector.head<3> () &&
          p_vector[0] > min_b_[0] && p_vector[1] > min_b_[1] && p_vector[2] > min_b_[2] &&
          p_vector[0] < max_b_[0] && p_vector[1] < max_b_[1] && p_vector[2] < max_b_[2])
      {
        Eigen::Vector4i p_ijk = ((p_vector - min_b_) * inverse_bin_size_).unaryExpr (std::ptr_fun (floor)).template cast<int> ();

        int pattern_idx = p_ijk.dot (bin_mult_);
        if (pattern[pattern_idx] == 0)
          pattern[pattern_idx] = 1;
          
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointInT, typename PointOutT> bool
pcl::SBPEstimation<PointInT, PointOutT>::initCompute ()
{
  // K not allowed for search
  if (k_)
  {
    PCL_ERROR ("[pcl::%s::initCompute] Use of K for local neighbourhood search not allowed. Please use search radius instead.\n", getClassName ().c_str ());
    return (false);
  }
  
  // Calculate bin size based on cloud dimensions (WACV 2016 version)
  if (use_cloud_dimensions_)
  {
    // If no search surface has been defined, use the input dataset as the search surface itself
    if (!surface_)
    {
      fake_surface_ = true;
      surface_ = input_;
    }
    
    Eigen::Vector4f min_pt, max_pt;
    pcl::getMinMax3D(*surface_, min_pt, max_pt);
    Eigen::Vector4f surface_dim = max_pt - min_pt;
    surface_dim[3] = 0;
    PCL_DEBUG ("[pcl::%s::initCompute] Number of points %d\n", getClassName ().c_str (), surface_->size ());
    PCL_DEBUG ("[pcl::%s::initCompute] Surface dimensions %f, %f, %f\n", getClassName ().c_str (), surface_dim[0], surface_dim[1], surface_dim[2]);
  
    float diag = surface_dim.norm ();
    bin_size_ = diag / std::pow(static_cast<float> (surface_->size ()), 1.0f/3);
    search_radius_ = bin_size_ * nr_bins_ * std::sqrt (3.0f) / 2;
    PCL_DEBUG ("[pcl::%s::initCompute] Calculated bin size of %f\n", getClassName ().c_str (), bin_size_);
    PCL_DEBUG ("[pcl::%s::initCompute] Calculated search radius of %f\n", getClassName ().c_str (), search_radius_);
  }
  
  // Check parameters
  if (!search_radius_)
  {
    PCL_ERROR ("[pcl::%s::initCompute] Search radius not defined.\n", getClassName ().c_str ());
    return (false);
  }
    
  if (!bin_size_) // If search radius is set, bin size should also be set
  {
    PCL_ERROR ("[pcl::%s::initCompute] Bin size not defined.\n", getClassName ().c_str ());
    return (false);
  }
  
  // Init compute
  if (!Feature<PointInT, PointOutT>::initCompute ())
  {
    PCL_ERROR ("[pcl::%s::initCompute] Init failed.\n", getClassName ().c_str ());
    return (false);
  }
  
  // Initialize extra parameters
  inverse_bin_size_ = 1 / bin_size_;
  float half_grid_size = bin_size_ * nr_bins_ / 2;
  min_b_ = Eigen::Vector4f::Zero ().array () - half_grid_size;
  max_b_ = Eigen::Vector4f::Zero ().array () + half_grid_size;
  min_b_[3] = 1;
  max_b_[3] = 1;
  
  return (true);
}

#define PCL_INSTANTIATE_SBPEstimation(T,OutT) template class PCL_EXPORTS pcl::SBPEstimation<T,OutT>;
#endif    // PCL_FEATURES_IMPL_SBP_H_
